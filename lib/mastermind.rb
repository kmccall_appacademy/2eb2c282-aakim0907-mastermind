class Code
  attr_reader :pegs

  PEGS = {"r"=>"red", "g"=>"green", "b"=>"blue", "y"=>"yellow", "o"=>"orange", "p"=>"purple"}

  def initialize(peg_arr)
    @pegs = peg_arr
  end

  def self.parse(str)
    parsed_code = str.downcase.chars
    if parsed_code.any? { |ele| !PEGS.keys.include?(ele) }
      raise "Includes invalid color"
    end
    Code.new(parsed_code)
  end

  def self.random
    random_code = []
    4.times { random_code << PEGS.keys.sample }
    Code.new(random_code)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other_code)
    exact_match = 0
    (0...4).each do |i|
      exact_match += 1 if self[i] == other_code[i]
    end
    exact_match
  end

  def near_matches(other_code)
    near_match = []
    other_code.pegs.each_with_index do |col, i|
      if @pegs.include?(col) && self[i] != other_code[i]
        near_match << col
      end
    end
    near_match.uniq.count
  end

  def ==(other_code)
    return false unless other_code.is_a?(Code)
    self.exact_matches(other_code) == 4 ? true : false
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
    @turn = 0
  end

  def play_game
    puts "Let's play <Mastermind>!!! You get 10 turns. Good Luck:)"
    until @turn == 10
      count_turn
      @guess_code = get_guess
      until @guess_code.pegs.length == 4
        puts "*You should guess 4 colors!"
        puts
        @guess_code = get_guess
      end
      self.display_matches(@guess_code)
      break if @secret_code == @guess_code
      sleep 1
    end
    conclude
  end

  def count_turn
    @turn += 1
    puts "***Turn #{@turn}***"
  end

  def get_guess
    puts "*Make a guess! (ex. 'RBGY' for 'Red-Blue-Green-Yellow')"
    puts "*Colors to choose from: [Red, Blue, Green, Yellow, Orange, Purple]"
    guess = gets.chomp
    Code.parse(guess)
  end

  def display_matches(code)
    puts
    puts "exact matches: #{@secret_code.exact_matches(code)} (Same position, same color)"
    puts "near matches: #{@secret_code.near_matches(code)} (Different position, same color)"
    puts
  end

  def conclude
    if @secret_code == @guess_code
      puts "Congratulations:) You Won!!! The code was #{@secret_code.uppercase}"
    else
      puts "Sorry:( This is the end of the game. The code was #{@secret_code.pegs.join.upcase}"
    end
  end
end

#### Uncomment below to play the game #####
# if __NAME__ = $PROGRAM_NAME
#   g = Game.new
#   g.play_game
# end
